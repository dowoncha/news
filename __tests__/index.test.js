import React from 'react'
import Index from '../pages/Index'

import { render, cleanup } from '@testing-library/react'

describe('<Index />', () => {
  afterEach(() => {
    cleanup()
  })

  it('should render', () => {
    const result = render(<Index topHeadlines={[]} />)

    result.getByText("The News")
  })
})