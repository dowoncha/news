import React from 'react'
import https from 'https'

import Head from 'next/head'

const NEWS_API_KEY = process.env.REACT_APP_NEWS_API_KEY

interface Article {
  source?: {
    id?: string,
    name?: string
  },
  author?: string,
  title?: string,
  description?: string,
  url?: string,
  urlToImage?: string,
  publishedAt?: string,
  content?: string
}

function Index({ topHeadlines = [] }: { topHeadlines: Article[] }) {
  return (
    <div>
      <Head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
      </Head>
      <div>
        <div className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title">
                The News
              </h1>
            </div>
          </div>
        </div>
        <div className="container">
          <p>{topHeadlines.length} Articles</p>
          <ul>
            {topHeadlines.map(article => {
              return (
                <li key={article.title}>
                  <a href={article.url}><img src={article.urlToImage} width="300px"/></a>
                  <a href={article.url}><h2>{article.title}</h2></a>
                  <p><small>By: {article.author}</small></p>
                  <p>{article.description}</p>
                  <p><small>Source: {article.source.name}</small></p>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
    </div>
  )
}

Index.getInitialProps = async ({ req }) => {
  const response: any = await new Promise((resolve) => {
    const request = https.get(`https://newsapi.org/v2/top-headlines?country=us&apiKey=${NEWS_API_KEY}`, (response) => {
      let buffer = ""

      response.on('data', (chunk) => {
        buffer += chunk.toString('utf8')
      })

      response.on('end', () => {
        let response_json = JSON.parse(buffer)

        resolve(response_json);
      })

      request.end()
    });
  })

  return { topHeadlines: response.articles }
}

export default Index